Instrucciones para Instalacion y Configuración
----------------------------------------------

---------------------------------------
El archivo para crear la base de datos es: bdtextil.sql que se encuentra dentro del directorio "backup"
---------------------------------------

---------------------------------------
Dentro da pasta 'Application' -> 'Config' edite o arquivo 'config.php' na seguinte linha.
Dentro del directorio 'Aplicacion' -> 'Config' en el archivo 'config.php' modifique
la linea que muestra:

$config['base_url']	= '';

Aqui debe colocar la url basada en su aplicacion, se debe colocar la raiz del servidor. Ejemplo:
$config['base_url']	= 'http://127.0.0.1'; ó 'http://dominio.com'

Si estas en un 'localhost' se puede colocar el directorio donde estan los programas
así:
$config['base_url']	= 'http://127.0.0.1/factutextil';

En algunos casos en un ambiente local (localhost) es necesario especificar un puerto
Ejemplo:
$config['base_url']	= 'http://127.0.0.1:3000/factutextil';
---------------------------------------


---------------------------------------
Dentro del directorio 'Application' -> 'Config' edite el archivo 'database.php' y coloque los datos de acesso a la base de datos.
---------------------------------------

---------------------------------------
El logotipo se encuentra dentro del directorio 'assets' -> 'img'. En caso que quieras cambiarlo, basta substituir el archivo del logo con uno con el mismo nombre (logo.png).
---------------------------------------

---------------------------------------
Datos de acesso
Email: admin@admin.com
Senha: admin
---------------------------------------


Cualquier duda ó problema contacte -> doble_a71@yahoo.com

<link href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<div class="row-fluid" style="margin-top: 0">
    <div class="span4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Reportes Rápidos</h5>
            </div>
            <div class="widget-content">
                <ul class="site-stats">
                    <li><a href="<?php echo base_url()?>index.php/reportes/clientesRapid"><i class="icon-user"></i> <small>Todos los Clientes</small></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="span8">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Reportes Personalizados</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">
                    <form action="<?php echo base_url()?>index.php/reportes/clientesCustom" method="get">
                    <div class="span4">
                        <label for="">Registro desde:</label>
                        <input type="text" id="dataInicial" name="dataInicial" class="datepicker span12" autocomplete="off" />
                    </div>
                    <div class="span4">
                        <label for="">Hasta:</label>
                        <input type="text" id="dataFinal" name="dataFinal" class="datepicker span12" autocomplete="off" />
                    </div>
                    <div class="span4">
                        <label for="">.</label>
                        <button class="btn btn-inverse span12"><i class="icon-print icon-white"></i> Imprimir</button>
                    </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script>
    $("#dataInicial").datepicker({ dateFormat: 'dd-mm-yy' });
    $("#dataFinal").datepicker({ dateFormat: 'dd-mm-yy' });
</script>
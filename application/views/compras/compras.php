<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'aVenda')){
				echo '
						<a href="'.base_url().'index.php/compras/adicionar" class="btn btn-success pull-right" title="Haga clic aqui para agregar productos comprados al inventario">
								<i class="icon-plus icon-white"></i> Comprar
						</a>
						<br>
				';
				}
?>
<?php

if(!$results){?>
	<div class="widget-box">
		 <div class="widget-title">
				<span class="icon">
						<i class="icon-tags"></i>
				 </span>
				<h5>Compras</h5>
		 </div>
		<div class="widget-content nopadding">
			<table class="table table-bordered ">
					<thead>
							<tr style="backgroud-color: #2D335B">
									<th># Fact.</th>
									<th>Fecha de Compra</th>
									<th>Proveedor</th>
									<th>Celular</th>
									<th>Nro Fact. Compra</th>
									<th>Monto</th>
									<th></th>
							</tr>
					</thead>
					<tbody>

							<tr>
									<td colspan="6">Ninguna Compra Registrada</td>
							</tr>
					</tbody>
			</table>
		</div>
	</div>

<?php } else{?>

<div class="widget-box">
	<?php
		echo $this->session->flashdata('mansaje');// este es para mostrar los mensajes q vienen del controlador despues de efectuar operaciones en las vistas
	?>
	 <div class="widget-title">
			<span class="icon">
					<i class="icon-tags"></i>
			 </span>
			<h5>Compras</h5>
	 </div>

		<div class="widget-content nopadding">
			<table class="table table-bordered ">
					<thead>
							<tr style="backgroud-color: #2D335B">
									<th># Fact.</th>
									<th>Fecha de Compra</th>
									<th>Proveedor</th>
									<th>Celular</th>
									<th>Nro Fact. Compra</th>
									<th>Monto</th>
									<th></th>
							</tr>
					</thead>
					<tbody>

							<?php
								// funcion para invertir las fechas
								function cambfecha($fecha) {
								  $fech = explode('-',$fecha);
								  $fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];

								  return $fnac;
								}

								foreach ($results as $r) {
									$fechaCompra = cambfecha($r->fechaCompra);
									echo '<tr>';
									echo '<td style="text-align:center">'.$r->idCompras.'</td>';
									echo '<td style="text-align:center">'.$fechaCompra.'</td>';
									echo '<td><a href="'.base_url().'index.php/proveedores/visualizar/'.$r->idProv.'">'.$r->nomProv.'</a></td>';
									echo '<td style="text-align:center">'.$r->celular.'</td>';
									echo '<td style="text-align:center">'.$r->nrofactcompra.'</td>';
									echo '<td style="text-align:center">'.number_format($r->valorTotal,2,',','.').'</td>';

									echo '<td>';
									if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')){
											echo '<a style="margin-right: 1%" href="'.base_url().'index.php/reportes/rpt_factura_pdf/'.$r->idCompras.'" class="btn tip-top" target="_blank" title="Ver Factura"><i class="icon-eye-open"></i></a>';
									}
									if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eVenda')){
											echo '<a style="margin-right: 1%" href="'.base_url().'index.php/compras/editar/'.$r->idCompras.'" class="btn btn-info tip-top" title="Editar Compra"><i class="icon-pencil icon-white"></i></a>';
									}
									/*if($this->permission->checkPermission($this->session->userdata('permisos_id'),'dVenda')){
											echo '<a href="#modal-excluir" role="button" data-toggle="modal" compra="'.$r->idCompras.'" class="btn btn-danger tip-top" title="Borrar Compra"><i class="icon-remove icon-white"></i></a>';
									}*/

									echo '</td>';
									echo '</tr>';
								}
							?>
							<tr>

							</tr>
					</tbody>
			</table>
		</div>
</div>

<?php echo $this->pagination->create_links();}?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form action="<?php echo base_url() ?>index.php/compras/excluir" method="post" >
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h5 id="myModalLabel">Excluir Compra</h5>
	</div>
	<div class="modal-body">
		<input type="hidden" id="idVenta" name="id" value="" />
		<h5 style="text-align: center">Desea realmente borrar esta Compra?</h5>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button class="btn btn-danger">Excluir</button>
	</div>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		 $(document).on('click', 'a', function(event) {

					var venta = $(this).attr('venta');
					$('#idVenta').val(venta);

			});

	});

</script>
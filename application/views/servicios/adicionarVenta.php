<link href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<div class="row-fluid" style="margin-top:0">
		<div class="span12">
				<div class="widget-box">
						<div class="widget-title">
								<span class="icon">
										<i class="icon-tags"></i>
								</span>
								<h5>Registro de Venta</h5>
						</div>
						<div class="widget-content nopadding">

								<div class="span12" id="divProdutosServicos" style=" margin-left: 0">
										<ul class="nav nav-tabs">
												<li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalles de la Venta</a></li>
										</ul>
										<div class="tab-content">
												<div class="tab-pane active" id="tab1">

														<div class="span12" >
																<?php if($custom_error == true){ ?>
																<div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Datos incompletos, verifique los campos con asterisco y seleccione correctamente cliente y vendedor</div>
																<?php } ?>
																<form action="<?php echo current_url(); ?>" method="post" id="form_Ventas" >

																	<div class="span11" style="padding-left: 3%">
																		<label for="fechaVenta">Fecha de Venta:<span class="required">*</span>
																			<input id="fechaVenta" class="datepicker span3" type="text" name="fechaVenta" value="<?php echo strftime("%d-%m-%Y"); ?>"  />
																			<input type="hidden" name="nrotemp" id="nrotemp" value="<?php echo $nrotemp;?>">
																		</label>
																	</div>

																	<div class="span11" style="padding: 1%">
																		<div class="span4">
																				<label for="cliente">Cliente<span class="required">*</span></label>
																				<input id="cliente" class="span12" type="text" name="cliente" value=""  />
																				<input id="clientes_id" class="span12" type="hidden" name="clientes_id" value=""  />
																		</div>
																		<div class="span4">
																				<label for="tlfno">Telefono<span class="required">*</span></label>
																				<input id="tlfno" class="span12" type="text"  name="tlfno" value=""  />
																		</div>
																		<div class="span4">
																				<label for="celu">Celular<span class="required">*</span></label>
																				<input id="celu" class="span12" type="text" name="celu" value=""  />
																		</div>
																	</div>

																	<div class="span11" style="padding: 1%">
																		<div class="span8 ui-widget">
																			<label>Nombre del Producto</label>
																				<input type="text" name="descriProd" id="descriProd" class="span12" maxlength="80" onchange="javascript:this.value=this.value.toUpperCase();" autocomplete="off"/>
																				<input id="codprod" class="span12" type="hidden" name="codprod" value="" />
																				<input id="unidad" class="span12" type="hidden" name="unidad" value="" />
																		</div>
																		<div class="span2">
																			<label>Precio:</label>
																				<input type="text" name="precioVenta" id="precioVenta" class="span12 text-center" maxlength="16" autocomplete="off"/>
																		</div>
																		<div class="span1">
																			<label>Cant.:</label>
																				<input type="text" name="cantpedido" id="cantpedido" class="span12 text-center" maxlength="6" onchange="javascript:this.value=this.value.toUpperCase();" autocomplete="off"/>
																		</div>

																		<div class="span1" style="padding-top:24px;">
																				<button type="button" id="agregar_art" class="btn btn-success" title="Agrega el articulo que seleccionaste al Servicio"><i class="fa fa-plus"></i> Agregar</button>
																		</div>
																	</div>

																	<?php echo $this->session->flashdata('mensaje');?>

																	<div class="span11" style="padding: 1%;">
																		<div class="span6">

																			<button type="submit" name="cancelar" id="cancelar" class="btn btn-danger pull-left" value="Cancelar Orden" dir="<?php echo base_url()?>index.php/servicios/cancelar_servicio" title="Haz Clic aqui si ya agregaste Articulos para Eliminarlos y salir de esta Factura"> Borrar Factura</button>
																		</div>
																		<div class="span6">

																			<button type="submit" name="registrar" id="registrar" class="btn btn-primary pull-right" value="Registrar Datos" dir="<?php echo base_url()?>index.php/servicios/insertar_servicio" title="Haz Clic aqui para Registrar esta Factura y Salir">&nbsp;&nbsp; Facturar &nbsp;&nbsp;</a>
																		</div>
																	</div>

																	<div class="span11" style="padding: 1%;">
																		<div id="msgerrores"></div>
																	</div>

																	<div class="span11" style="padding: 1%;">
																		<table id="grilla" class="table table-striped table-bordered table-condensed" role="grid">
																			<thead>
																				<tr class="bg-success">
																					<th class="text-center">  Item                </th>
																					<th class="text-center">  Descripción del Articulo despachado</th>
																					<th class="text-center">  Cantidad             </th>
																					<th class="text-center">  Unidad	             </th>
																					<th class="text-center">  Categoria            </th>
																					<th class="text-center">  Costo Total          </th>
																					<th></th>
																				</tr>
																			</thead>
																			<tbody id="tbody_listar" style="vertical-align:middle; text-align:center;"> </tbody>
																		</table>
																	</div>

																	<!--<div class="span11" style="padding: 1%; margin-left: 0">
																		<div class="span6 offset3" style="text-align: center">
																			<button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Facturar</button>
																			<a href="<?php //echo base_url() ?>index.php/vendas" class="btn"><i class="icon-arrow-left"></i> Volver</a>
																		</div>
																	</div>-->

																</form>
														</div>

												</div>

										</div>

								</div>.
						</div>
				</div>
		</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div id="ventanaBorrar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
				<h2> Borrando..! <?php echo $this->input->post('idFacturas');?></h2>
			</div>
		</div>
	</div>
</div>
<!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
<div id="ventanaEnviando" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="ct text-center">
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.AreYouSure/jquery.are-you-sure.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.AreYouSure/ays-beforeunload-shim.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script>

		$("#cliente").autocomplete({
			source: "<?php echo base_url(); ?>index.php/servicios/autoCompleteCliente",
			minLength: 1,
			select: function( event, ui ) {
			 	$("#clientes_id").val(ui.item.id);
			 	$("#tlfno").val(ui.item.tlfn);
			 	$("#celu").val(ui.item.celu);
			}
		});

		$("#descriProd").autocomplete({
			source: "<?php echo base_url(); ?>index.php/servicios/autoCompleteProducto",
			minLength: 1,
			select: function( event, ui ) {
				$("#descriProd").val(ui.item.descriProd);
				$("#precioVenta").val(ui.item.preciov);
				$("#cantpedido").val('1');
				$("#unidad").val(ui.item.unidad);
				$("#codprod").val(ui.item.codprod);
			}
		});

		$("#form_Ventas").validate({
				rules:{
					 cliente: {required:true},
					 tecnico: {required:true},
					 fechaVenta: {required:true}
				},
				messages:{
					 cliente: {required: 'Campo Requerido.'},
					 tecnico: {required: 'Campo Requerido.'},
					 fechaVenta: {required: 'Campo Requerido.'}
				},

					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
							$(element).parents('.control-group').addClass('error');
					},
					unhighlight: function(element, errorClass, validClass) {
							$(element).parents('.control-group').removeClass('error');
							$(element).parents('.control-group').addClass('success');
					}
		});

		$("#fechaVenta" ).datepicker({ dateFormat: 'dd-mm-yy' });
</script>
<script>
	// para agregar articulo por articulo al servicio
	$('#agregar_art').on('click', function(event) {
		// compruebo que se envien datos para grabar via ajax
		if($("#cliente").val().length < 1) {
			alert("Cliente no puede estar vacio\n");
			$("#cliente").focus();
			return;
		}
		if($("#tlfno").val().length < 1) {
			alert("Telefono no puede estar vacio\n");
			$("#tlfno").focus();
			return;
		}
		if($("#celu").val().length < 1) {
			alert("Celular no puede estar vacio\n");
			$("#celu").focus();
			return;
		}


		if($("#descriProd").val().length < 1) {
			alert("Descripción no puede estar vacio\n");
			$("#descriProd").focus();
			return;
		}
		if($("#cantpedido").val().length < 1) {
			alert("Cantidad no puede estar vacio\n");
			$("#cantpedido").focus();
			return;
		}
		if($("#precioVenta").val().length < 1) {
			alert("Precio no puede estar vacio\n");
			$("#precioVenta").focus();
			return;
		}
		if($("#unidad").val() == '') {
			alert("Unidad no puede estar vacio\n");
			$("#unidad").focus();
			return;
		}
		// recibo los datos y los convierto en variables para pasarlos a php
		var idtmp = $('#nrotemp').val();
		var codpr = $('#codprod').val();
		var cantp = $('#cantpedido').val();
		var prcio = $('#precioVenta').val();
		var unidd = $('#unidad').val();
		var dataString = { idtmp:idtmp, codpr:codpr, cantp:cantp, prcio:prcio, unidd:unidd };
		$.ajax({
			url: "<?php echo base_url()?>index.php/servicios/agregar_art",
			type: "POST",
			data:dataString,
				success: function(data) {
					//limpio los campos de donde envie los datos a ser guardados
					$('#codprod').val('');
					$('#descriProd').val('');
					$('#cantpedido').val('');
					$('#precioVenta').val('');
					$('#unidad').val('');
					// muestro otra vez la lista de datos
					fn_buscar();
					$("#descriProd").focus();
				},
				error: function(err) {
					alert('agregar_art: '+err);
				}
		});
	});
</script>
<script>
	// para ejecutar la accion de mostrar los articuloas a listar
	function fn_buscar(){
		var idtmp = $('#nrotemp').val();
		var dataString =  { idtmp:idtmp };

		$.ajax({
			url: '<?php echo base_url()?>index.php/servicios/listar_articulos',
			type: 'POST',
			data: dataString,
			success: function(data){
				$("#tbody_listar").html(data);
				$("#descriProd").focus();
			},
			error: function(err) {
				alert('fn_buscar'+err);
			}
		});
	}
</script>
<script>
	// cuando haga clic en el boton borrar
	$('#ventanaBorrar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idart = button.data('borrar');
		var dataBorra = { idart:idart };

		$.ajax({
			url: "<?php echo base_url()?>index.php/servicios/borrar_articulo",
			type: "POST",
			data: dataBorra,
			success: function (data) {
				$("#ventanaBorrar").modal("hide");
				fn_buscar();
			},
			error: function(err) {
				alert(err);
			}
		});
	});
</script>
<script>
	// para cargar el div_listar cuando cargue la pagina
	if (document.addEventListener){
		window.addEventListener('load',fn_buscar,false);
	} else {
		window.attachEvent('onload',fn_buscar);
	}
</script>
<script>
	$(document).bind('keydown',function(e){
		if ( e.which == 27 ) {
			alert('Si deseas salir de la Factura, debes presionar el Boton "Borrar Factura"');
		};
	});
</script>
<script>
	// para enviar el formulario segun el boton que al que se le haga clic
	$("button[type=submit]").click(function() {
		// compruebo que se envien datos para grabar via ajax
		if($("#cliente").val().length < 1) {
			alert("Cliente no puede estar vacio\n");
			$("#cliente").focus();
			return;
		}
		if($("#tlfno").val().length < 1) {
			alert("Telefono no puede estar vacio\n");
			$("#tlfno").focus();
			return;
		}
		if($("#celu").val().length < 1) {
			alert("Celular no puede estar vacio\n");
			$("#celu").focus();
			return;
		}

		var accion = $(this).attr('dir');
			$("#ventanaEnviando").delay(200).fadeOut(300, function(){
				$(".ct").append("Espere, por favor..!");
			});
			$('form').attr('action', accion);
			$('form').submit();
	});
</script>
<script>
	//$('#form_Ventas').areYouSure( {
	//	'message':'Si quieres salir y ya agregaste un producto, debes presionar el boton "Borrar Factura".!'
	//});
</script>
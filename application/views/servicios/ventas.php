<?php if($this->permission->checkPermission($this->session->userdata('permisos_id'),'aVenda')){
				echo '
						<a href="'.base_url().'index.php/servicios/adicionar" class="btn btn-success" title="Haga clic aqui para Vender Productos a Clientes">
								<i class="icon-plus icon-white"></i> Vender
						</a>
				';
				}
?>
<?php

if(!$results){?>
	<div class="widget-box">
		 <div class="widget-title">
				<span class="icon">
						<i class="icon-tags"></i>
				 </span>
				<h5>Ventas</h5>
		 </div>
		<div class="widget-content nopadding">
			<table class="table table-bordered ">
					<thead>
							<tr style="backgroud-color: #2D335B">
									<th>#</th>
									<th>Fecha de Venta</th>
									<th>Cliente</th>
									<th>Facturado</th>
									<th></th>
							</tr>
					</thead>
					<tbody>

							<tr>
									<td colspan="6">Ninguna venta Registrada</td>
							</tr>
					</tbody>
			</table>
		</div>
	</div>

<?php } else{?>

<div class="widget-box">
	<?php
		echo $this->session->flashdata('mansaje');// este es para mostrar los mensajes q vienen del controlador despues de efectuar operaciones en las vistas
	?>
	 <div class="widget-title">
			<span class="icon">
					<i class="icon-tags"></i>
			 </span>
			<h5>Ventas</h5>
	 </div>

		<div class="widget-content nopadding">
			<table class="table table-bordered ">
					<thead>
							<tr style="backgroud-color: #2D335B">
									<th># Fact.</th>
									<th>Fecha de Venta</th>
									<th>Cliente</th>
									<th>Celular</th>
									<th>Facturado</th>
									<th>Monto</th>
									<th></th>
							</tr>
					</thead>
					<tbody>

							<?php
								// funcion para invertir las fechas
								function cambfecha($fecha) {
								  $fech = explode('-',$fecha);
								  $fnac = $fech[2].'-'.$fech[1].'-'.$fech[0];

								  return $fnac;
								}

								foreach ($results as $r) {
									$fechaVenta = cambfecha($r->fechaVenta);
									if($r->facturado == 1){$facturado = 'Si';} else{ $facturado = 'No';}
									echo '<tr>';
									echo '<td style="text-align:center">'.$r->idVentas.'</td>';
									echo '<td style="text-align:center">'.$fechaVenta.'</td>';
									echo '<td><a href="'.base_url().'index.php/clientes/visualizar/'.$r->idClientes.'">'.$r->nomCliente.'</a></td>';
									echo '<td style="text-align:center">'.$r->celular.'</td>';
									echo '<td style="text-align:center">'.$facturado.'</td>';
									echo '<td style="text-align:center">'.number_format($r->valorTotal,2,',','.').'</td>';

									echo '<td>';
									if($this->permission->checkPermission($this->session->userdata('permisos_id'),'vVenda')){
											echo '<a style="margin-right: 1%" href="'.base_url().'index.php/reportes/rpt_factura_pdf/" class="btn tip-top" id="repopdf" name="idvnt" target="_blank" title="Ver Factura" rel="'.$r->nrotemp_id.'"><i class="icon-eye-open"></i></a>';
									}
									if($this->permission->checkPermission($this->session->userdata('permisos_id'),'eVenda')){
											echo '<a style="margin-right: 1%" href="'.base_url().'index.php/servicios/editar/'.$r->nrotemp_id.'" class="btn btn-info tip-top" title="Editar Venta"><i class="icon-pencil icon-white"></i></a>';
									}
									if($this->permission->checkPermission($this->session->userdata('permisos_id'),'dVenda')){
											echo '<a href="#modal-excluir" role="button" data-toggle="modal" venta="'.$r->nrotemp_id.'" class="btn btn-danger tip-top" title="Borrar Venta"><i class="icon-remove icon-white"></i></a>';
									}

									echo '</td>';
									echo '</tr>';
								}
							?>
							<tr>

							</tr>
					</tbody>
			</table>
		</div>
</div>

<?php echo $this->pagination->create_links();}?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<form action="<?php echo base_url() ?>index.php/ventas/excluir" method="post" >
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h5 id="myModalLabel">Excluir Venta</h5>
	</div>
	<div class="modal-body">
		<input type="hidden" id="idVenta" name="id" value="" />
		<h5 style="text-align: center">Desea realmente borrar esta Venta?</h5>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		<button class="btn btn-danger">Excluir</button>
	</div>
	</form>
</div>

<!--<script type="text/javascript">
	$(document).ready(function(){

		 $(document).on('click', 'a', function(event) {

					var venta = $(this).attr('venta');
					$('#idVenta').val(venta);

			});

	});

</script>-->
<script>
	 var form = document.createElement("form"); // crear un form
		with(form) {
		setAttribute("name", "myform"); //nombre del form
		setAttribute("action", ""); // action por defecto
		setAttribute("method", "post"); // method POST }
		}
		var input = document.createElement("input"); // Crea un elemento input
		with(input) {
		setAttribute("name", "repopdf"); //nombre del input que va a pasar el valor a la otra pagina
		setAttribute("type", "hidden"); // tipo hidden
		setAttribute("value", ""); // valor por defecto
		}

		form.appendChild(input); // añade el input al formulario
		document.getElementsByTagName("body")[0].appendChild(form); // añade el formulario al documento

		window.onload=function(){
			var my_links = document.getElementsByTagName("a");
			for (var a = 0; a < my_links.length; a++) {
				if (my_links[a].name=="idvnt") my_links[a].onclick = function() {
					document.myform.action=this.href;
					document.myform.repopdf.value=this.rel;
					document.myform.submit();
					return false;
				}
			}
		}
</script>
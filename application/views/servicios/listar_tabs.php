<?php
	// si hay registros
	$listar = $this->Main_model->listar_tabulados();
	if($listar->num_rows() > 0) {
		foreach ($listar->result() as $r) {
			$btn_borrar = array (
				'name' 			=> 'borrar_tab',
				'id' 				=> 'borrar_tab',
				'type' 			=> 'button',
				'content' 		=> '<i class="fa fa-minus"></i>',
				'class' 			=> 'btn btn-xs btn-danger',
				'data-borrar'	=> $r->idtab,
				'title' 			=> 'Borrar este Concepto',
				'data-toggle' 	=> 'modal',
				'data-target' 	=>'#ventanaBorrar'
			);
			//<button type='button' id='borrar_art' class='btn btn-xs btn-danger' data-borrar='".$r->idserv."' title='Borrar este articulo' data-toggle='modal' data-target='#ventanaBorrar' ><i class='fa fa-minus'></i></button>

			echo "
				<tr>
					<td>".$r->idtab."</td>
					<td>".$r->tipoart."</td>
					<td>".$r->descripserv."</td>
					<td>".$r->preciounit."</td>
					<td>
						".form_button($btn_borrar)."
					</td>
				</tr>
			";
		}// fin del foreach

	}
?>

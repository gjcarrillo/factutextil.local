<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	// Incluimos el archivo fpdf
	require_once APPPATH."/third_party/vendor/autoload.php";
 
//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Mpdf extends Mpdf\Mpdf {
	
	public function __construct() {
		parent::__construct();
	}

	function pdf_create($html, $filename, $stream)
	{

	    $mpdf = new \Mpdf\Mpdf();

	    $mpdf->WriteHTML($html);

	    if ($stream)
	    {
	        $mpdf->Output($filename . '.pdf', 'D');
	    }
	    else
	    {
	        $mpdf->Output('./uploads/temp/' . $filename . '.pdf', 'F');

	        return './uploads/temp/' . $filename . '.pdf';
	    }  

	}


}